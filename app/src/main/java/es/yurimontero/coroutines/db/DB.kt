package es.yurimontero.coroutines.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import es.yurimontero.coroutines.db.dao.MainDao
import es.yurimontero.coroutines.vo.Todo

@Database(
        entities = [Todo::class],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class DB : RoomDatabase() {

    abstract fun mainDao(): MainDao

}
