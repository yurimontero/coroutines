package es.yurimontero.coroutines.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import es.yurimontero.coroutines.vo.Todo

@Dao
interface MainDao {

    @Insert
    suspend fun add(list: List<Todo>)

    @Query("SELECT * FROM Todo")
    suspend fun get(): List<Todo>

    @Query("DELETE FROM Todo")
    suspend fun del()

    @Transaction
    suspend fun clearAndInsert(list: List<Todo>) {
        del()
        add(list)
    }
}
