package es.yurimontero.coroutines.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import es.yurimontero.coroutines.R
import es.yurimontero.coroutines.di.Injectable
import es.yurimontero.coroutines.utils.SAdapter
import es.yurimontero.coroutines.viewmodel.MainViewModel
import es.yurimontero.coroutines.vo.Todo
import kotlinx.android.synthetic.main.fragment_home.*
import timber.log.Timber
import javax.inject.Inject

class HomeFragment : Fragment(), Injectable, AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id) {
            R.id.spinner -> {
                Timber.wtf(parent.getItemAtPosition(position).toString())
            }
        }
    }

    companion object {
        fun newInstance() = HomeFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel

    private val todoAdapter by lazy {
        activity?.let {
            SAdapter<Todo>(it, {
                text1.text = String.format("${item?.id}. ${item?.title}")
            }, {
                text1.text = String.format("${item?.id}. ${item?.title}")
            })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]

        spinner.apply {
            adapter = todoAdapter
            onItemSelectedListener = this@HomeFragment
            prompt = "Select your favourite language"
        }

        viewModel.list.observe(this, Observer {
            todoAdapter?.items = it
        })

        viewModel.progressStatus.observe(this, Observer {
            showProgress(it ?: false)
        })

        viewModel.init()

    }

    private fun showProgress(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

}
