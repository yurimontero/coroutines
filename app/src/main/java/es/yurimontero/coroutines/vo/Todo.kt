package es.yurimontero.coroutines.vo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Todo(
    @PrimaryKey val id: Int = 0,
    val title: String = "",
    val completed: Boolean = false
)