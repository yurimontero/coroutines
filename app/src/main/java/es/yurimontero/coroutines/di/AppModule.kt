package es.yurimontero.coroutines.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import es.yurimontero.coroutines.App
import es.yurimontero.coroutines.utils.AppExecutor
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module(
        includes = [
            ActivityModule::class,
            DbModule::class,
            FragmentModule::class,
            NetModule::class,
            ViewModelModule::class]
)
class AppModule {

    @Singleton
    @Provides
    fun provideSharedPreferences(app: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Singleton
    @Provides
    fun provideAppExecutors(): AppExecutor {
        return AppExecutor(
                Executors.newFixedThreadPool(3),
                Executors.newSingleThreadExecutor(),
                AppExecutor.MainThreadExecutor()
        )
    }

    @Singleton
    @Provides
    fun provideApp(app: Application): App {
        return app as App
    }

}
