package es.yurimontero.coroutines.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import es.yurimontero.coroutines.db.DB
import javax.inject.Singleton

@Module
class DbModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): DB {
        return Room
            .databaseBuilder(app, DB::class.java, "DB.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideMainDao(db: DB) = db.mainDao()

}
