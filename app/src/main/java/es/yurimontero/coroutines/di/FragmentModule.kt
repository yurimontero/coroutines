package es.yurimontero.coroutines.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import es.yurimontero.coroutines.ui.fragments.HomeFragment

@Suppress("unused")
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

}
