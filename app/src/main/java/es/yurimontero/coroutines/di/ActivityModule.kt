package es.yurimontero.coroutines.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import es.yurimontero.coroutines.ui.MainActivity

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

}
