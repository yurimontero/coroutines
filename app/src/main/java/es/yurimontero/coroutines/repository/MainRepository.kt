package es.yurimontero.coroutines.repository

import es.yurimontero.coroutines.api.WebService
import es.yurimontero.coroutines.db.dao.MainDao
import es.yurimontero.coroutines.vo.Todo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    private val webService: WebService,
    private val mainDao: MainDao
) {

    suspend fun getTodo() = webService.getTodo()

    suspend fun addTodos(list: List<Todo>) {
        mainDao.clearAndInsert(list)
    }

    suspend fun getTodos() = mainDao.get()

}
