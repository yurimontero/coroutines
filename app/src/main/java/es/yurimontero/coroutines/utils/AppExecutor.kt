package es.yurimontero.coroutines.utils

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor

class AppExecutor(
        private val diskIO: Executor,
        private val networkIO: Executor,
        private val mainThread: Executor
) {

    fun diskIO(): Executor {
        return diskIO
    }

    fun networkIO(): Executor {
        return networkIO
    }

    fun mainThread(): Executor {
        return mainThread
    }

    class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }

}
