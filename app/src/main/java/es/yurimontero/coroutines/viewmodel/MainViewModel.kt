package es.yurimontero.coroutines.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.yurimontero.coroutines.repository.MainRepository
import es.yurimontero.coroutines.vo.Todo
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() {

    private val _progressStatus = MutableLiveData(false)
    private val _todos = MutableLiveData<List<Todo>>()

    val list: LiveData<List<Todo>> = _todos
    val progressStatus: LiveData<Boolean> = _progressStatus

    fun init() {
        viewModelScope.launch {
            setProgressStatus(true)
//            delay(2000)
            val list = repository.getTodo()
            addParameters(list)
            setProgressStatus(false)
        }
    }

    private fun setProgressStatus(isProgressIndicatorShown: Boolean) {
        _progressStatus.value = isProgressIndicatorShown
    }

    private suspend fun addParameters(list: List<Todo>) {
        repository.addTodos(list)
        _todos.value = repository.getTodos()
    }

}
