package es.yurimontero.coroutines.api

import es.yurimontero.coroutines.vo.Todo
import retrofit2.http.GET

interface WebService {

    @GET("todos")
    suspend fun getTodo(): List<Todo>

}